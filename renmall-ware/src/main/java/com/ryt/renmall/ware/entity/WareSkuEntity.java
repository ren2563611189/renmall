package com.ryt.renmall.ware.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品库存
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 11:17:15
 */
@Data
@TableName("wms_ware_sku")
public class WareSkuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private Long skuId;
	/**
	 * 
	 */
	private Long wareId;
	/**
	 * 
	 */
	private Integer stock;
	/**
	 * 
	 */
	private String skuName;
	/**
	 * 
	 */
	private Integer stockLocked;

}
