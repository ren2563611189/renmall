package com.ryt.renmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RenmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(RenmallWareApplication.class, args);
    }

}
