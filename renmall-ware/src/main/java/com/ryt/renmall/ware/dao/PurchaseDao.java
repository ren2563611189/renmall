package com.ryt.renmall.ware.dao;

import com.ryt.renmall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 11:17:15
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
