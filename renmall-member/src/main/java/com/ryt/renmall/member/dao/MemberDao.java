package com.ryt.renmall.member.dao;

import com.ryt.renmall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 10:51:49
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
