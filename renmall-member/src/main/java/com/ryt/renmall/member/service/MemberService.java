package com.ryt.renmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ryt.common.utils.PageUtils;
import com.ryt.renmall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 10:51:49
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

