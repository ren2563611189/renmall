package com.ryt.renmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.ryt.renmall.member.feign")
@SpringBootApplication
public class RenmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(RenmallMemberApplication.class, args);
    }

}
