package com.ryt.renmall.member.feign;

import com.ryt.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("renmall-coupon")   //表示要调用哪个服务
public interface CouponFeignService {
    @RequestMapping("/coupon/coupon/member/list")
    public R memberCoupons();

    @RequestMapping("/coupon/coupon/test")
    public R test();

}
