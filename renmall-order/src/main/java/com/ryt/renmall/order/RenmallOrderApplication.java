package com.ryt.renmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RenmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(RenmallOrderApplication.class, args);
    }

}
