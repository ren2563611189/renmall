package com.ryt.renmall.order.dao;

import com.ryt.renmall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 11:02:52
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
