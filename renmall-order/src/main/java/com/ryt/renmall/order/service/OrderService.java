package com.ryt.renmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ryt.common.utils.PageUtils;
import com.ryt.renmall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 11:02:52
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

