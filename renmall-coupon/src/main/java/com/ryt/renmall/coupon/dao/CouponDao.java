package com.ryt.renmall.coupon.dao;

import com.ryt.renmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 00:06:14
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
