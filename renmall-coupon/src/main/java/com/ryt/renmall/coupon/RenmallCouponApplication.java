package com.ryt.renmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class RenmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(RenmallCouponApplication.class, args);
    }
//    spring.cloud.nacos.discovery.namespace=ba2c9fe8-08bc-4dff-9a3f-af21898c0955
//    spring.cloud.nacos.config.namespace=ba2c9fe8-08bc-4dff-9a3f-af21898c0955  #更换成prop环境即开发环境
//    spring.cloud.nacos.config.group=618
}
