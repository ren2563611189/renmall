package com.ryt.renmall.coupon.dao;

import com.ryt.renmall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 00:06:13
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
