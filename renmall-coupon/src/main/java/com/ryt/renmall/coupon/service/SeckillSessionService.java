package com.ryt.renmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ryt.common.utils.PageUtils;
import com.ryt.renmall.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 00:06:13
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

