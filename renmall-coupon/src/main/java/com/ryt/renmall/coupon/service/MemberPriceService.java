package com.ryt.renmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ryt.common.utils.PageUtils;
import com.ryt.renmall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-25 00:06:13
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

