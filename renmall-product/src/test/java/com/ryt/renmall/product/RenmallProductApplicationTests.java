package com.ryt.renmall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ryt.renmall.product.entity.BrandEntity;
import com.ryt.renmall.product.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
class RenmallProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Test
    public void selectById(){
        /*添加数据*/
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setName("华为NB");
//        brandService.save(brandEntity);
//        log.info("保存成功。。。");

        /*更新数据*/
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setBrandId(1L);
//        brandEntity.setDescript("中华有为");
//        brandService.updateById(brandEntity);

        /*查询数据*/
//        BrandEntity byId = brandService.getById(1L);
//        System.out.println("查询成功。。。。");
//        System.out.println(byId);

        List<BrandEntity> brand_id = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        brand_id.forEach(a -> System.out.println(a));


    }

}
