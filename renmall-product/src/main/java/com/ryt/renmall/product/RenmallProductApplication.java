package com.ryt.renmall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RenmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(RenmallProductApplication.class, args);
    }

}
