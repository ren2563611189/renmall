package com.ryt.renmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ryt.common.utils.PageUtils;
import com.ryt.renmall.product.entity.SkuSaleAttrValueEntity;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-24 18:41:23
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

