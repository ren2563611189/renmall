package com.ryt.renmall.product.dao;

import com.ryt.renmall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author renyatao
 * @email 2563611189@qq.com
 * @date 2023-11-24 18:41:24
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
