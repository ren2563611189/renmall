package com.ryt.renmall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
@Configuration
public class renmallCorsConfiguration {
    @Bean
    public CorsWebFilter corsConfiguration() {

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        //创建这个对象，所有跟跨域有关的都放到这里
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //允许任意请求头
        corsConfiguration.addAllowedHeader("*");
        //允许任意的请求方式  get 、head 、post
        corsConfiguration.addAllowedMethod("*");
        //允许任意源地址请求
        corsConfiguration.addAllowedOriginPattern("*");
        //是否允许携带cookie跨域  否则就会丢失跨域相关的cookie信息
        corsConfiguration.setAllowCredentials(true);


        source.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsWebFilter(source);
    }
}
