package com.ryt.renmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RenmallGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(RenmallGatewayApplication.class, args);
    }

}
